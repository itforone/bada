const Sequelize = require("sequelize");

module.exports = class User extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        user_id: {
          type: Sequelize.STRING(255),
          allowNull: true,
          unique: true,
          defaultValue:'',
          comment:'회원아이디'  
        },
        user_password: {
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원비밀번호'
        },
        user_name:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원이름'
        },
          user_hp:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원휴대폰번호'
        },
          user_1:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보1'
        },user_2:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보2'
        },user_3:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보3'
        },user_4:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보4'
        },user_5:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보5'
        },user_6:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보6'
        },user_7:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보7'
        },user_8:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보8'
        },user_9:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보9'
        },user_10:{
            type: Sequelize.STRING(255),
            allowNull: true,
            defaultValue: '',
            comment: '회원정보10'
        },
        access_token:{
          type:Sequelize.STRING(255),
          allowNull:true,
          defaultValue:'',
          comment:'액세스토큰'
        },
        refresh_token:{
          type:Sequelize.STRING(255),
          allowNull:true,
          defaultValue:'',
          comment:'리프레시토큰'
        },
        user_level:{
          type: Sequelize.INTEGER.UNSIGNED,
          allowNull: true,
          defaultValue: 1,
          comment: '회원등급 1:준회원 2:정회원 10:관리자'
        }
        
      },
      {
        sequelize,
        timestamps: true,
        underscored: false,
        modelName: "User",
        tableName: "users",
        paranoid: true,
        charset: "utf8",
        collate: "utf8_general_ci",
      }
    );
  }

  static associate(db) {}
};
