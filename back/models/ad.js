const Sequelize = require("sequelize");

module.exports = class Ad extends Sequelize.Model {
    static init(sequelize) {
        return super.init(
            {
                user_id: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue:'',
                    comment:'회원아이디'
                },
                ad_title: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '광고명'
                },
                ad_image:{
                    type: Sequelize.TEXT('long'),
                    allowNull: true,
                    comment: '광고이미지'
                },
                ad_url:{
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '광고URL 주소'
                }

            },
            {
                sequelize,
                timestamps: true,
                underscored: false,
                modelName: "Ad",
                tableName: "ad",
                paranoid: true,
                charset: "utf8",
                collate: "utf8_general_ci",
            }
        );
    }

    static associate(db) {}
};
