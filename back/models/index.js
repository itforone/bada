const Sequelize = require("sequelize");
const User = require("./user");
const Ad = require("./ad");
const env = process.env.NODE_ENV || "development";
const config = require("../config/config")[env]; //db정보가 있는 곳
const db = {}; //db 테이블을 객체로 받는 곳

const sequelize = new Sequelize( //db화
  config.database,
  config.username,
  config.password,
  config
);

db.sequelize = sequelize;
db.User = User; //db 모델명 설정
db.Ad = Ad;

User.init(sequelize);
User.associate(db);
Ad.init(sequelize);
Ad.associate(db);


module.exports = db; //db 모듈을 내보내기
