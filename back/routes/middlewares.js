const jwt = require("jsonwebtoken");
const {User} = require("../models");
exports.isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.status(401).send('로그인이 필요합니다.');
    }
};

exports.isNotLoggedIn = (req, res, next) => {
    if (!req.isAuthenticated()) {
        next();
    } else {
        res.status(401).send('로그인하지 않은 사용자만 접근 가능합니다.');
    }
};

exports.jwtMiddleware = (req,res,next) => {
    let access_token = req.cookies.access_token;
    let refresh_token = req.cookies.refresh_token;
    jwt.verify(access_token, process.env.ACCESS_TOKEN_SECRET, async (err,decoded) => {

       if (err) {
           console.log("error");
           jwt.verify(refresh_token, process.env.REFRESH_TOKEN_SECRET, async (err,decoded) => {
               if (err) {
                   return res.status(401).json({ isAuth: false, message: "token decode 실패" });
               }
               console.log(decoded);
               //액세스 토큰 값 재발급하기
               let re_access_token=jwt.sign(
                   {
                       type: "ACCESS_TOKEN",
                       user_id: decoded.user_id,
                       id:decoded.id
                   },
                   process.env.ACCESS_TOKEN_SECRET,
                   {
                       expiresIn: "1d",
                       issuer: "액세스토큰",
                   }
               );
               //토큰값 업데이트
               await User.update({
                   access_token:re_access_token,

               },{
                   where:{id:decoded.id}
               });
               const user = await User.findOne({
                   where:{
                       id:decoded.id
                   }
               });
               console.log(user);
               req.user=user;
               next();
           });
           return;
       }

       const user = await User.findOne({
           where:{
               id:decoded.id
           }
       });
       req.user=user;
       next();
    });
}
