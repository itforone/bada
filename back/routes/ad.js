const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const router = express.Router();
const {User,Ad} = require("../models");
const {isLoggedIn,isNotLoggedIn,jwtMiddleware} = require('./middlewares');
try {
    fs.readdirSync('uploads');
} catch (error) {
    console.error('uploads 폴더가 없어 uploads 폴더를 생성합니다.');
    fs.mkdirSync('uploads');
}
const upload = multer({
    storage: multer.diskStorage({
        destination(req, file, cb) {
            console.log(req);
            cb(null, 'uploads/');
        },
        filename(req, file, cb) {
            const ext = path.extname(file.originalname);
            cb(null, path.basename(file.originalname, ext) + Date.now() + ext);
        },
    }),
    limits: { fileSize: 5 * 1024 * 1024 },
});
//광고목록 보여주기
router.get("/", async (req,res,next) => {
   try {
       //페이지 관련
       const cntRow = await Ad.findAndCountAll();
       const totalCount = cntRow.count;//광고 총갯수
       const page = req.query.page || 1;
       const limit = 10;
       const pageSize = 10;//보여주는 페이지 수
       const fromRecord = (page-1) * limit;//limit 첫 번째 수
       const totalPage = Math.ceil(totalCount/limit);//총 페이지 수
       const startPage = (Math.ceil(page/pageSize) -1 ) * pageSize + 1;//페이지 그룹에 첫번째 페이지
       let lastPage = startPage + pageSize +1;//마지막 페이지
       lastPage=totalPage < lastPage?totalPage:lastPage;
       const fromRecordPage = [];//한 그룹에 보여줄 페이지
       for(let i = startPage-1; i < lastPage; i++){
           fromRecordPage.push(i);
       }
       //프론트엔드에 보낼 페이징 데이터
       const pages = {
         pageSize:pageSize,
         fromRecord:fromRecord,
         totalPage:totalPage,
         startPage:startPage,
         lastPage:lastPage,
         fromRecordPage:fromRecordPage,
         page:parseInt(page)
       };
       const row = await Ad.findAll({
           limit:[fromRecord,limit],
           order:[['id','desc']]
       });
       if(row){
           return res.status(200).json({row:row,pages:pages});
       }else{
           return res.status(200).json(null);
       }

   } catch (err){
       console.error(err);
       return res.send(err);
   }
});

//파일 업로드하기
router.post('/upload', upload.single('img'), (req, res) => {
    console.log(req.file.filename);
    return res.status(200).send(req.file.filename);
    //res.json({ url: `/img/${req.file.filename}` });
});
//광고등록
router.post("/add",jwtMiddleware,async (req,res,next) => {
   console.log(req.body);
   try{
       console.log(req.user);
       await Ad.create({
          user_id:req.user.user_id,
          ad_title:req.body.ad_title,
          ad_image:req.body.ad_image,
          ad_url:req.body.ad_url
       });
       //페이지 관련
       const cntRow = await Ad.findAndCountAll();
       const totalCount = cntRow.count;//광고 총갯수
       const page = req.body.page || 1;
       const limit = 10;
       const pageSize = 10;//보여주는 페이지 수
       const fromRecord = (page-1) * limit;//limit 첫 번째 수
       const totalPage = Math.ceil(totalCount/limit);//총 페이지 수
       const startPage = (Math.ceil(page/pageSize) -1 ) * pageSize + 1;//페이지 그룹에 첫번째 페이지
       let lastPage = startPage + pageSize +1;//마지막 페이지
       lastPage=totalPage < lastPage?totalPage:lastPage;
       const fromRecordPage = [];//한 그룹에 보여줄 페이지
       for(let i = startPage-1; i < lastPage; i++){
           fromRecordPage.push(i);
       }
       //프론트엔드에 보낼 페이징 데이터
       const pages = {
           pageSize:pageSize,
           fromRecord:fromRecord,
           totalPage:totalPage,
           startPage:startPage,
           lastPage:lastPage,
           fromRecordPage:fromRecordPage,
           page:parseInt(page)
       };
       const row = await Ad.findAll({
           limit:[fromRecord,limit],
           order:[['id','desc']]
       });
       return res.status(200).json({row:row,pages:pages});

   } catch (error){
        console.error(error);
   }
});

//광고수정
router.post("/update",jwtMiddleware,async (req,res,next) => {
    console.log(req.body);
    try{
        console.log(req.user);
        await Ad.update({
            ad_title:req.body.ad_title,
            ad_image:req.body.ad_image,
            ad_url:req.body.ad_url
        },{
            where:{id:req.body.id}
        });
        //페이지 관련
        const cntRow = await Ad.findAndCountAll();
        const totalCount = cntRow.count;//광고 총갯수
        const page = req.body.page || 1;
        const limit = 10;
        const pageSize = 10;//보여주는 페이지 수
        const fromRecord = (page-1) * limit;//limit 첫 번째 수
        const totalPage = Math.ceil(totalCount/limit);//총 페이지 수
        const startPage = (Math.ceil(page/pageSize) -1 ) * pageSize + 1;//페이지 그룹에 첫번째 페이지
        let lastPage = startPage + pageSize +1;//마지막 페이지
        lastPage=totalPage < lastPage?totalPage:lastPage;
        const fromRecordPage = [];//한 그룹에 보여줄 페이지
        for(let i = startPage-1; i < lastPage; i++){
            fromRecordPage.push(i);
        }
        //프론트엔드에 보낼 페이징 데이터
        const pages = {
            pageSize:pageSize,
            fromRecord:fromRecord,
            totalPage:totalPage,
            startPage:startPage,
            lastPage:lastPage,
            fromRecordPage:fromRecordPage,
            page:parseInt(page)
        };
        const row = await Ad.findAll({
            limit:[fromRecord,limit],
            order:[['id','desc']]
        });
        return res.status(200).json({row:row,pages:pages});

    } catch (error){
        console.error(error);
    }
});

//광고삭제
router.get("/remove",jwtMiddleware,async (req,res,next) => {
    console.log(req.body);
    try{
        console.log(req.user);
        await Ad.destroy({
            where:{
                id:req.query.id
            }
        })
        //페이지 관련
        const cntRow = await Ad.findAndCountAll();
        const totalCount = cntRow.count;//광고 총갯수
        let page = parseInt(req.query.page) || 1;
        const limit = 10;
        let fromRecord = (page-1) * limit;//limit 첫 번째 수

        //삭제시 없을 경우 page-1하기
        const chkRow = await Ad.findAll({
            limit:[fromRecord,limit],
            order:[['id','desc']]
        });
        console.log(chkRow.length);
        if(chkRow.length===0){
            page--;
            console.log(page);
            fromRecord = (page-1) * limit;//limit 첫 번째 수
        }

        const pageSize = 10;//보여주는 페이지 수

        const totalPage = Math.ceil(totalCount/limit);//총 페이지 수
        const startPage = (Math.ceil(page/pageSize) -1 ) * pageSize + 1;//페이지 그룹에 첫번째 페이지
        let lastPage = startPage + pageSize +1;//마지막 페이지
        lastPage=totalPage < lastPage?totalPage:lastPage;
        const fromRecordPage = [];//한 그룹에 보여줄 페이지
        for(let i = startPage-1; i < lastPage; i++){
            fromRecordPage.push(i);
        }

        //프론트엔드에 보낼 페이징 데이터
        const pages = {
            pageSize:pageSize,
            fromRecord:fromRecord,
            totalPage:totalPage,
            startPage:startPage,
            lastPage:lastPage,
            fromRecordPage:fromRecordPage,
            page:parseInt(page)
        };
        const row = await Ad.findAll({
            limit:[fromRecord,limit],
            order:[['id','desc']]
        });
        return res.status(200).json({row:row,pages:pages});

    } catch (error){
        console.error(error);
        return res.send(error);
    }
});

module.exports = router; // 라우터 내보내기