const express = require("express");
const {User} = require("../models");
const passport = require('passport');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const {jwtMiddleware} = require("./middlewares");
router.get("/",jwtMiddleware, async (req,res,next) => {

    try {

        if(req.user) {
            const row = await User.findOne({
                where: {id: req.user.id}
            });
            return res.status(200).json(row);
        }else {
            return res.status(200).send(null);
        }

    }catch (error){
        console.log("로그인 중");
        console.error(error);
        return res.status(200).json(error);
        next(error);
    }
});
//회원가입 db 인서트 및 업데이트
router.post("/register", async (req, res,next) => {
    try{

        if(req.body.mode!="update") {
            const row = await User.findOne({
                where: {
                    user_id: req.body.user_id
                }
            });
            if (row) {
                return res.status(403).send('사용중인 아이디 입니다.');
            }
            const hashPassword = await bcrypt.hash(req.body.user_password, 12);

            await User.create({
                user_id: req.body.user_id,
                user_name: req.body.user_name,
                user_hp: req.body.user_hp,
                user_level: 2,
                user_password: hashPassword
            });
        }else{
            //회원정보 업데이트 하기

            const hashPassword = req.body.user_password !== ""&&req.body.user_password!==null? await bcrypt.hash(req.body.user_password, 12):"";
            let field = {
                user_name:req.body.user_name,
                user_hp:req.body.user_hp
            };
            if(hashPassword!==""){
                field.user_password=hashPassword;
            }
            console.log(field);
            const row=await User.update(field,{
                where:{user_id:req.body.user_id}
            }).then((result) =>{
                console.log(result);
            });
            console.log(row);

        }
        return res.status(200).send('ok');
    }catch(e){
      console.log(e);
    }
});
//아이디 중복체크
router.get("/id_check", async (req,res,next) => {
    try{
        console.log(req.query.user_id);
        console.log("아이디 중복체크");
        const row =await User.findOne({
            where:{user_id:req.query.user_id}
        });
        if(row){
            res.json({is_check:false});
        }else{
            res.json({is_check:true});
        }
    }catch(e){
        console.log(e)
    }
});
//회원로그인
router.post("/login",  async (req,res,next) => {
    try {

        passport.authenticate('local',(err,user,info) => {

            if (err) {
                console.error(err);
                return next(err);
            }
            if (info) {
                console.log(info.reason);
                return res.status(401).send(info.reason);
            }

            return req.login(user, async (loginErr) => {

                if (loginErr) {
                    console.error(loginErr);
                    return next(loginErr);
                }


                let row = user;
                //액세스 토큰 값 발급하기
                const access_token = jwt.sign(
                    {
                        type: "ACCESS_TOKEN",
                        user_id: row.user_id,
                        id:row.id
                    },
                    process.env.ACCESS_TOKEN_SECRET,
                    {
                        expiresIn: "1d",
                        issuer: "액세스토큰",
                    }
                );
                //리프레시 토큰 값 발급하기
                const refresh_token = jwt.sign(
                    {
                        type: "REFRESH_TOKEN",
                        user_id: row.user_id,
                        id:row.id
                    },
                    process.env.REFRESH_TOKEN_SECRET,
                    {
                        expiresIn: "30 days",
                        issuer: "리프레시토큰",
                    }
                );
                console.log(access_token);
                //토큰값 업데이트
                await User.update({
                    access_token:access_token,
                    refresh_token:refresh_token
                },{
                    where:{user_id:user.user_id}
                });

                return res.status(200).json(user);
            });
        })(req, res, next);
        console.log("로그인중")
    } catch (err){
        console.log(err);
    }
});

router.get("/logout",async (req,res,next) => {
    console.log("로그아웃");
    try {
        console.log("로그아웃");
        req.logout();
        req.session.destroy();
        res.send('ok');
    }catch (error){
        console.log("로그아웃 실패");
        console.error(error);
        res.send(error);
    }
});

module.exports = router; // 라우터 내보내기