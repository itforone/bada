import produce from "../util/produce";
export const initalState = {
    uploadRequest:false,//파일업로드 요청
    adImage:null,//이미지 파일 경로
    uploadDone:false,//파일업로드 완료
    uploadError:null,//파일업로드 후 오류 메세지
    uploadLoading:false,//파일업로드 로딩중

    adFormRequest:false,//광고 등록요청 할 때
    adFormSuccess:false,//광고 등록 요청이 성공할 때
    adFormLoading:false,//광고 요청시 로딩
    adFormFail:false,//광고 등록 요청이 실패할 때
    adFormError:null,//광고 등록 요청이 오류가 날 때

    adFormUpdateRequest:false,//광고 수정요청 할 때
    adFormUpdateSuccess:false,//광고 수정 요청이 성공할 때
    adFormUpdateLoading:false,//광고 수정 요청시 로딩
    adFormUpdateFail:false,//광고 수정 요청이 실패할 때
    adFormUpdateError:null,//광고 수정 요청이 오류가 날 때

    adRemoveRequest:false,//광고 삭제요청 할 때
    adRemoveSuccess:false,//광고 삭제 요청이 성공할 때
    adRemoveLoading:false,//광고 삭제 요청시 로딩
    adRemoveFail:false,//광고 삭제 요청이 실패할 때
    adRemoveError:null,//광고 삭제 요청이 오류가 날 때


    loadAdListLoading:false,//광고목록 불러오기
    loadAdListDone:false,//광고목록 불러오기 성공 여부
    loadAdListError:null,//광고목록 불러오기 할 때 오류원인
    ads:[],//광고 목록 배열
    pages:[],//페이징
}
//이미지 첨부 할 때 상태
export const UPLOAD_REQUEST = 'UPLOAD_REQUEST';//파일 업로드 요청시
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';//파일 업로드 완료시
export const UPLOAD_FAIL = 'UPLOAD_FAIL';//파일 업로드 오류가 날 시

//광고등록 할 때 상태
export const AD_ADD_REQUEST = 'AD_ADD_REQUEST';//광고 등록 요청시
export const AD_ADD_SUCCESS = 'AD_ADD_SUCCESS';//광고 등록 완료시
export const AD_ADD_FAIL = 'AD_ADD_FAIL';//광고등록 오류가 날 시
//광고수정 할 때 상태
export const AD_UPDATE_REQUEST = 'AD_UPDATE_REQUEST';//광고수정 요청시
export const AD_UPDATE_SUCCESS = 'AD_UPDATE_SUCCESS';//광고수정 완료시
export const AD_UPDATE_FAIL = 'AD_UPDATE_FAIL';//광고수정 오류가 날 시

//광고수정 할 때 상태
export const AD_REMOVE_REQUEST = 'AD_REMOVE_REQUEST';//광고삭제 요청시
export const AD_REMOVE_SUCCESS = 'AD_REMOVE_SUCCESS';//광고삭제 완료시
export const AD_REMOVE_FAIL = 'AD_REMOVE_FAIL';//광고삭제 오류가 날 시

//광고 목록 불러오기 할 때 상태
export const AD_LIST_REQUEST = 'AD_LIST_REQUEST';//광고 목록 요청을 할 때
export const AD_LIST_SUCCESS = 'AD_LIST_SUCCESS';//광고 목록 요청 완료
export const AD_LIST_FAIL = 'AD_LIST_FAIL';//광고 목록 요청할 때 오류가 날 시


const reducer = (state =initalState, action ) => produce(state, (draft) => {
    switch (action.type){
        //파일 업로드 요청시
        case UPLOAD_REQUEST:
            draft.uploadRequest = true;
            draft.uploadDone=false;
            draft.uploadLoading = true;
            draft.adImage = null;
            break;
        case UPLOAD_SUCCESS:
            draft.uploadRequest = false;
            draft.uploadDone=true;
            draft.uploadLoading = false;
            draft.adImage = action.data;
            break;
        case UPLOAD_FAIL:
            draft.uploadRequest = false;
            draft.uploadDone=false;
            draft.uploadLoading = false;
            draft.uploadError = action.error;
            break;
        //광고 등록
        case AD_ADD_REQUEST:
            draft.adFormRequest = true;
            draft.adFormDone=false;
            draft.adFormLoading = true;
            draft.adImage = null;
            break;
        case AD_ADD_SUCCESS:
            draft.adFormRequest = false;
            draft.adFormDone=true;
            draft.adFormLoading = false;
            draft.adImage=null;
            draft.ads = action.data.row;
            draft.pages = action.data.pages;
            break;
        case AD_ADD_FAIL:
            draft.adFormRequest = false;
            draft.adFormDone=false;
            draft.adFormLoading = false;
            draft.adFormError = action.error;
            break;
        //광고 수정
        case AD_UPDATE_REQUEST:
            draft.adFormUpdateRequest = true;
            draft.adFormUpdateDone=false;
            draft.adFormUpdateLoading = true;
            draft.adImage = null;
            break;
        case AD_UPDATE_SUCCESS:
            draft.adFormUpdateRequest = false;
            draft.adFormUpdateDone=true;
            draft.adFormUpdateLoading = false;
            draft.adImage=null;
            draft.ads = action.data.row;
            draft.pages = action.data.pages;
            break;
        case AD_UPDATE_FAIL:
            draft.adFormUpdateRequest = false;
            draft.adFormUpdateDone=false;
            draft.adFormUpdateLoading = false;
            draft.adFormUpdateError = action.error;
            break;
        //광고 삭제
        case AD_REMOVE_REQUEST:
            draft.adRemoveRequest = true;
            draft.adRemoveDone=false;
            draft.adRemoveLoading = true;
            draft.adImage = null;
            break;
        case AD_REMOVE_SUCCESS:
            draft.adRemoveRequest = false;
            draft.adRemoveDone=true;
            draft.adRemoveLoading = false;
            draft.ads = action.data.row;
            draft.pages = action.data.pages;
            break;
        case AD_REMOVE_FAIL:
            draft.adRemoveRequest = false;
            draft.adRemoveDone=false;
            draft.adRemoveLoading = false;
            break;
        //광고 목록
        case AD_LIST_REQUEST:
            draft.loadAdListLoading = true;
            draft.loadAdListDone=false;
            break;
        case AD_LIST_SUCCESS:
            draft.loadAdListDone=true;
            draft.loadAdListLoading=false;
            if(action.data.row) {
                draft.ads = action.data.row;
                draft.pages = action.data.pages;
            }
        case AD_LIST_FAIL:
            draft.loadAdListDone=false;
            draft.loadAdListLoading=false;
        default:
            break;
    }
});

export default reducer;
