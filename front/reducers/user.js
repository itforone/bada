import produce from "../util/produce";
import {setCookie} from "../util";
//여기에 immer도 포함되어있음
//immer는 불변성 좀 더 쉽게 해주는 것

export const initalState = {
    //회원가입을 할 때
    registerLoading:false,//회원가입 요청시 로딩
    registerDone:false,//회원가입을 할 때
    registerSuccess:null,//회원가입이 성공이 될 떼
    registerError:null,//회원가입을 할 때 오류가 발생할 때
    //아이디 중복 체크
    userIdSuccess:null,//아이디 중복체크 완료
    userIdCheckMessage:null,//아이디 중복체크 메세지
    //로그인을 할 때
    loginLoading:false,
    loginDone:false,
    loginError:null,
    //로그아웃을 할 때
    logoutLoading:false,
    logoutDone:false,
    logoutError:null,
    //로그인 한 후에 회원 정보가져오기
    loadUserLoading:false,
    loadUserDone:false,
    loadUserError:null,
    user:null,//로그인 후 회원정보 담는 변수
}

//회원가입을 할 때 상태
export const REGISTER_FAIL = 'REGISTER_FAIL';//회원가입 실패할때
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';//회원가입 성공할 때
export const REGISTER_REQUEST = 'REGISTER_REQUEST';//회원가입 요청을 할 때

//아이디 중복 체크
export const USER_ID_SUCCESS = 'USER_ID_SUCCESS';
export const USER_ID_REQUEST = 'USER_ID_REQUEST';
export const USER_ID_FAIL = 'USER_ID_FAIL';

//로그인
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
//로그아웃
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAIL = 'LOGOUT_FAIL';
//로그인 후 정보가져오기
export const LOAD_USER_REQUEST = 'LOAD_USER_REQUEST';
export const LOAD_USER_SUCCESS = 'LOAD_USER_SUCCESS';
export const LOAD_USER_FAIL = 'LOAD_USER_FAIL';
export const loginRequestAction = (data) => ({
    type: LOGIN_REQUEST,
    data,
});

const reducer = (state =initalState, action ) => produce(state, (draft) => {

    switch (action.type){
        //회원가입에 요청을 할 때
        case REGISTER_REQUEST:
            draft.registerLoading = true;
            draft.registerDone = false;
            draft.registerSuccess=null;
            break;
        //회원 가입에 성공을 할 때
        case REGISTER_SUCCESS:
            draft.registerLoading = false;
            draft.registerDone = true;
            draft.registerSuccess = true;
            break;
        //회원 가입에 실패를 할 때
        case REGISTER_FAIL:
            draft.registerError=action.error;
            draft.registerLoading = false;
            break;
        //아이디 중복체크 요청을 할 때
        //아이디 중복체크 요청을 할 때에는 딱히 할 것이 없기 때문에 그대로 둠
        case USER_ID_REQUEST:
            draft.userIdSuccess=null;
            break;
        //아이디 중복체크시 중복 아이디가 없을 경우
        case USER_ID_SUCCESS:
            draft.userIdSuccess = true;
            draft.userIdCheckMessage=action.data;
            break;
        //아이디 중복체크시 중복 아이디가 있을 경우
        case USER_ID_FAIL:
            draft.userIdSuccess=false;
            draft.userIdCheckMessage=action.data;
            break;
        //로그인 요청을 할 때
        case LOGIN_REQUEST:
            draft.loginLoading=true;
            draft.loginDone = false;
            draft.loginError=null;
            break;
        //로그인 성공을 할 때
        case LOGIN_SUCCESS:
            draft.loginLoading=false;
            draft.loginDone=true;
            draft.user = action.data;
            break;
        //로그인 실패를 할 때
        case LOGIN_FAIL:
            draft.loginLoading=false;
            draft.loginDone=false;
            draft.loginError=action.error;
            console.log(draft.logoutError);
            break;
        //로그아웃 요청을 할 때
        case LOGOUT_REQUEST:
            draft.logoutLoading=true;
            draft.logoutDone = false;
            break;
        //로그아웃 성공을 할 때
        case LOGOUT_SUCCESS:
            draft.logoutLoading=false;
            draft.logoutDone=true;
            draft.loginDone = false;
            draft.user=null;
            //로그아웃이 될 때는 쿠키에 있는 토큰값을 초기화를 해야 한다
            setCookie("access_token","");
            setCookie("refresh_token","");
            break;
        //로그아웃 실패를 할 때
        case LOGOUT_FAIL:
            draft.logoutLoading=false;
            draft.logoutDone=false;
            draft.logoutError=action.error;

            break;
        //로그인 정보 요청을 할 때
        case LOAD_USER_REQUEST:
            draft.loadUserLoading=true;
            draft.loadUserDone = false;
            break;
        //로그인 정보 성공을 할 때
        case LOAD_USER_SUCCESS:
            draft.loadUserLoading=false;
            draft.loadUserDone=true;
            draft.user=action.data;
            break;
        //로그인 정보 실패를 할 때
        case LOAD_USER_FAIL:
            draft.loadUserLoading=false;
            draft.loadUserDone=false;
            draft.loadUserError="";
            draft.user=null;
            break;
        default:
            break;
    }
});
export default reducer;