import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import axios from 'axios';
//결과 처리를 하기 위한 변수
//REDUCER에서 쓰임
import {
    UPLOAD_REQUEST,
    UPLOAD_FAIL,
    UPLOAD_SUCCESS,
    AD_ADD_REQUEST,
    AD_ADD_SUCCESS,
    AD_ADD_FAIL,
    AD_LIST_REQUEST,
    AD_LIST_SUCCESS,
    AD_LIST_FAIL,
    AD_UPDATE_REQUEST,
    AD_UPDATE_SUCCESS,
    AD_UPDATE_FAIL,
    AD_REMOVE_REQUEST,
    AD_REMOVE_SUCCESS,
    AD_REMOVE_FAIL

} from "../reducers/ad";
//이미지 업로드
function adUploadApi(data){
    return axios.post("/ad/upload/",data);
}
function* adUpload(action){
    try{
        const res = yield call(adUploadApi,action.data);
        yield put({
            type:UPLOAD_SUCCESS,
            data:res.data
        });
    }catch (e){
        yield put({
            type:UPLOAD_FAIL,
            error:e.response.data
        });
    }
}
//파일첨부를 할 때
function* watchAdUpload(){
    yield takeLatest(UPLOAD_REQUEST,adUpload);
}
//광고등록을 할 때
function adAddApi(data){
    console.log(data);
    return axios.post("/ad/add/",data);
}
function* adAdd(action){
    try{
        const res = yield call(adAddApi,action.data);
        yield put({
            type:AD_ADD_SUCCESS,
            data:res.data
        });
    }catch (e){
        yield put({
            type:AD_ADD_FAIL,
            error:e.response.data
        });
    }
}
//광고 등록 할 때
function* watchAddAd(){
    yield takeLatest(AD_ADD_REQUEST,adAdd);
}

//광고 목록 요청을 할 때
function adListApi(data){
    console.log(data);
    return axios.get("/ad/?page="+data.page,data);
}
function* adList(action){
    try{
        const res = yield call(adListApi,action.data);
        yield put({
            type:AD_LIST_SUCCESS,
            data:res.data
        });
    }catch (e){
        yield put({
            type:AD_LIST_FAIL,
            error:e.response.data
        });
    }
}
//광고 목록 요청을 할 때
function* watchAdList(){
    yield takeLatest(AD_LIST_REQUEST,adList);
}
//광고 수정 요청을 할 때
function adUpdateApi(data){
    console.log(data);
    return axios.post("/ad/update/",data);
}
function* adUpdate(action){
    try{
        const res = yield call(adUpdateApi,action.data);
        yield put({
            type:AD_UPDATE_SUCCESS,
            data:res.data
        });
    }catch (e){
        yield put({
            type:AD_UPDATE_FAIL,
            error:e.response.data
        });
    }
}
//광고 수정 할 때
function* watchUpdateAd(){
    yield takeLatest(AD_UPDATE_REQUEST,adUpdate);
}

//광고 삭제 요청을 할 때
function adRemoveApi(data){
    console.log(data);
    return axios.get(`/ad/remove/?id=${data.id}&page=${data.page}`);
}
function* adRemove(action){
    try{
        const res = yield call(adRemoveApi,action.data);
        yield put({
            type:AD_REMOVE_SUCCESS,
            data:res.data
        });
    }catch (e){
        yield put({
            type:AD_REMOVE_FAIL,
            error:e.response.data
        });
    }
}
//광고 수정 할 때
function* watchRemoveAd(){
    yield takeLatest(AD_REMOVE_REQUEST,adRemove);
}
export default function* adSaga() {
    yield all([
        fork(watchAdUpload),
        fork(watchAddAd),
        fork(watchUpdateAd),
        fork(watchRemoveAd),
        fork(watchAdList),
    ]);
}