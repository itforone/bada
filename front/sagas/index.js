import { all, fork } from 'redux-saga/effects';
import axios from 'axios';
import userSaga from './user';
import adSaga from "./ad";

axios.defaults.baseURL = 'http://172.23.95.137:3061/';
axios.defaults.withCredentials = true;//로그인을 할때는 반드시 true로 할 것
axios.defaults.headers['Access-Control-Allow-Origin'] = '*';

export default function* rootSaga() {
  yield all([
    fork(userSaga),
    fork(adSaga)
  ]);
}
