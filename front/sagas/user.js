import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import axios from 'axios';
//결과 처리를 하기 위한 변수
//REDUCER에서 쓰임
import {
    REGISTER_FAIL,//회원가입 실패할때
    REGISTER_SUCCESS,//회원가입 성공할 때
    REGISTER_REQUEST,//회원가입 요청을 할 때
    USER_ID_REQUEST,//아이디 중복체크 요청할 때
    USER_ID_SUCCESS,//아이디 중복체크 요청이 응답받을 때
    USER_ID_FAIL,//아이디 중복체크 요청이 응답받지 않을 때
    LOGIN_REQUEST,//로그인 요청을 할 때
    LOGIN_FAIL,//로그인시 오류가 날 때
    LOGIN_SUCCESS,//로그인 성공을 할 때
    LOGOUT_REQUEST,//로그아웃 요청을 할 때
    LOGOUT_FAIL,//로그아웃시 오류가 날 때
    LOGOUT_SUCCESS, //로그아웃 성공을 할 때
    LOAD_USER_REQUEST,//회원정보가져오기 요청을 할 때
    LOAD_USER_SUCCESS,//회원정보가져오기 성공을 할 때
    LOAD_USER_FAIL,//회원정보가져오기 실패를 할 때

} from '../reducers/user';

//아이디 중복체크 URL 보내기
function userIdCheckApi(data) {
    return axios.get(`/user/id_check?user_id=${data.user_id}`);//userIdCheck에 리턴
}
//아이디 중복 체크하기
function* userIdCheck(action) {
    try{
        const res =yield call(userIdCheckApi, action.data);//앞에는 함수 뒤에는 파라미터
        //아이디 중복체크 결과값
        //결과값은 reducer로 넘어감
        if(res.data.is_check){
            yield put({
                type: USER_ID_SUCCESS,
                data:'사용하실 수 있는 아이디입니다'
            });
        }else{
            yield put({
                type: USER_ID_FAIL,
                data:'사용하실 수 없는 아이디입니다'
            });
        }
    }catch(e){
        console.error(e);
    }
} 
function* watchUserIdCheck(){
    yield takeLatest(USER_ID_REQUEST, userIdCheck)
}
//회원가입
function userRegisterApi(data){
    return axios.post('/user/register/',data);
}

function* userRegister(action){
    try{
        const res = yield call(userRegisterApi,action.data);
        yield put({
            type:REGISTER_SUCCESS,
            data:res.data
        });
    }catch(e){
        console.log(e);
        yield put({
           type:REGISTER_FAIL,
           error:e.response.data
        });
    }
}

function* watchUserRegister(){
    yield takeLatest(REGISTER_REQUEST,userRegister);
}

//회원로그인
function userLoginApi(data){
    return axios.post('/user/login/',data);
}

function* userLogin(action){
    try{
        console.log(action.data);
        const res = yield call(userLoginApi,action.data);
        yield put({
            type:LOGIN_SUCCESS,
            data:res.data
        });
    }catch(e){
        console.log(e);
        yield put({
            type:LOGIN_FAIL,
            error:e.response.data
        });
    }
}
function* watchUserLogin(){
    yield takeLatest(LOGIN_REQUEST,userLogin);
}
//회원로그아웃
function userLogoutApi(data){
    return axios.get('/user/logout',data);
}

function* userLogout(action){
    try{
        console.log(action.data);
        const res = yield call(userLogoutApi,action.data);
        yield put({
            type:LOGOUT_SUCCESS,
            data:res.data
        });
    }catch(e){
        console.log(e);
        yield put({
            type:LOGOUT_FAIL,
            error:e.response.data
        });
    }
}
function* watchUserLogout(){
    yield takeLatest(LOGOUT_REQUEST,userLogout);
}

//회원정보 가져오기
function userLoadApi(data){
    console.log(data);
    return axios.get(`/user`);
}

function* userLoad(action){
    
    try{

        const res = yield call(userLoadApi,action.data);
        yield console.log(action.data)
        yield put({
            type:LOAD_USER_SUCCESS,
            data:res.data
        });
    }catch(e){
        console.log(e);
        console.log("사용자 정보 가져오기 실패");
        yield put({
            type:LOAD_USER_FAIL,
            error:e.response.data
        });
    }
}
function* watchUserLoad(){
    yield takeLatest(LOAD_USER_REQUEST,userLoad);
}

export default function* userSaga() {
    yield all([
       fork(watchUserIdCheck),//아이디 중복체크
       fork(watchUserRegister),//회원가입
       fork(watchUserLogin), //회원로그인
       fork(watchUserLogout), //회원로그아웃
       fork(watchUserLoad), //회원정보가져오기
    ]);
}