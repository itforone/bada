import React, {useCallback} from "react";
import Link from "next/link";
import {useSelector,useDispatch} from "react-redux";
import {errorAlert} from "../util/sweetalert2";
import {LOGOUT_REQUEST} from "../reducers/user";
const LoginMenuLayout = ({user}) => {
    const dispatch = useDispatch();

    console.log(user);


    const onLogoutClick = useCallback(()=>{
        console.log(user);
        dispatch({type:LOGOUT_REQUEST});
    },[user]);
    return (
        <div>
            <ul id="tnb">
                <Link href="#">
                <a href="https://itforone.co.kr:443/~badasinwoo/bbs/password_lost.php" target="_blank"
                   id="login_password_lost"><i className="fal fa-search"></i> 아이디 비밀번호 찾기</a>
                </Link>
                <li>
                    {
                        user?
                        <Link href={`/register/?mode=update`}>
                            <a><i><span></span><span></span><span></span></i>회원수정</a>
                        </Link>
                            :
                        <Link href="/register">
                            <a><i><span></span><span></span><span></span></i>회원가입</a>
                        </Link>
                    }
                </li>
                <li>
                    {user?

                        <a  onClick={onLogoutClick}><b>로그아웃</b></a>

                    :
                    <Link href="/login">
                        <a><b>로그인</b></a>
                    </Link>}
                </li>
                {/*
                <li><a href="https://itforone.co.kr:443/~badasinwoo/adm"><b>관리자</b></a></li>
                <li><a href="https://itforone.co.kr:443/~badasinwoo/bbs/member_confirm.php?url=https://itforone.co.kr:443/~badasinwoo/bbs/register_form.php">정보수정</a></li>
                <li><a href="https://itforone.co.kr:443/~badasinwoo/bbs/logout.php">로그아웃</a></li>*/}
            </ul>
        </div>
    );
}

export default LoginMenuLayout;