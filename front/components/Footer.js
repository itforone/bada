import React from 'react';

const Footer = () => {
    return (
        <div>
            <div id="footer">
                <address>
                    <h1>
                        <a href="https://itforone.co.kr:443/~badasinwoo/bbs/content.php?co_id=company">
                            ㈜화린에스엠하우스 -
                            BADA<br/>분양정보 플랫폼 ㅣ BUNYANG ALL DATA ASSET
                        </a>
                    </h1>
                    <p><span><strong>주소 : </strong> 부산광역시 해운대구 센텀중앙로 97, B동 404호 (재송동, 센텀스카이비즈) </span>
                        <span><strong>대표 : </strong> 김우태, 고동국</span>
                        <span><strong>사업자등록번호 : </strong> 826-88-02072</span></p>
                    <p>
                        <span><strong></strong> </span>
                        <span><strong></strong> </span>
                        <span><strong></strong> </span>
                    </p>
                    <p className="co">COPYRIGHT(c) 2022 <strong>BADA. </strong> ALL RIGHTS RESERVED.</p>
                </address>
            </div>
        </div>
    );
}
export default Footer;