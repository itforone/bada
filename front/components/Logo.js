import React from "react";

const Logo = () => {
    return (
        <div>
            <div id="hd">
                <div id="logo">
                    <img src="/img/common/logo.png"/>
                </div>
            </div>
        </div>
    );
}

export default Logo;