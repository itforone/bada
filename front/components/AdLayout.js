import React, {useCallback, useEffect, useRef, useState} from "react";
import Link from "next/link";
import {Button, Form, Modal, Pagination} from "react-bootstrap";
import { FileUploader } from "react-drag-drop-files";
import {useDispatch, useSelector} from "react-redux";
import {AD_ADD_REQUEST, AD_LIST_REQUEST, AD_REMOVE_REQUEST, AD_UPDATE_REQUEST, UPLOAD_REQUEST} from "../reducers/ad";
import useInput from "../hooks/useInput";
import {confirmAlert, errorAlert} from "../util/sweetalert2";
import axios from "axios";
const fileTypes = ["JPG", "PNG", "GIF"];
import PropTypes from 'prop-types';

const AdLayout = () => {
    const [user_level,setUserLevel]=useState(0);
    const [show,setShow] = useState(false);//광고 등록 모달창 띄우기 여부
    const [is_upload,setIsUpload] = useState(false);//이미지 업로드 여부
    const [page,setPgage] = useState(1);
    const [editAd,setEditAd] = useState(null);
    const dispatch = useDispatch();
    const {adImage,ads,pages} = useSelector((state) => state.ad);
    const {user} =useSelector((state)=>state.user);
    const [ad_title,onChangeAdTitle,setAdTitle] = useInput('');
    const [ad_url,onChangeAdUrl,setAdUrl] = useInput('');
    const adTitleRef = useRef();
    const adUrlRef = useRef();

    //useEffect로 회원레벨이 변경이 됨
    useEffect(() =>{
        console.log(user);
        if(user!==null) {
            setUserLevel(user.user_level);
        }else{
            setUserLevel(0);
        }
    },[user]);
    //광고등록 버튼을 클릭을 할 때 모달창 보이게
    const onAdClick = () => {
        setEditAd(null);
        setAdTitle('');
        setAdUrl('');
        setShow(true);
    };
    //모달창 닫기를 눌렀을 시 모달창 안 보이게
    const onHideClick = () => {
        setShow(false);
        setIsUpload(false);
    };
    //페이징을 눌렀을 때
    const onAdListClick = useCallback((e) =>{
        const newPage=parseInt(e.target.text);
        console.log(newPage);
        setPgage(newPage);
        dispatch({
            type:AD_LIST_REQUEST,
            data:{
                page:newPage
            }
        });
    },[]);
    //광고 수정 버튼을 클릭할 때
    const onAdEditClick = useCallback((ad) => {
        setEditAd(ad);
        setAdTitle(ad.ad_title);
        setAdUrl(ad.ad_url);
        setShow(true);
    },[]);
    //광고 삭제를 눌렀을 때
    const onAdRemoveClick = useCallback((id) => {
        const confirm=confirmAlert('이 광고를 삭제하시겠습니까?','이 광고를 삭제하시겠습니까?');
        confirm.then((res) => {
            if(res.isConfirmed) {
                dispatch({
                    type: AD_REMOVE_REQUEST,
                    data: {
                        page: pages.page,
                        id: id
                    }
                });
            }else{
                errorAlert(null,"삭제 취소","삭제를 취소되었습니다.");
            }
        });

    },[pages]);
    /*function onAdEditClick (ad){
        console.log(ad);
    }*/


    //파일 첨부시 업로드하기
    const handleChange = useCallback((files) => {
       const imageFormData = new FormData();
       imageFormData.append('img',files);
       dispatch(({
           type:UPLOAD_REQUEST,
           data:imageFormData
       }));
        setIsUpload(true);
    });
    //광고등록 버튼을 눌렀을 때
    const onAdSubmitClick = useCallback(() => {
        if(ad_title.length < 1){
            errorAlert(adTitleRef,'광고명 입력 오류','광고명을 입력하세요');
            return;
        }
        if(editAd){
            dispatch({
               type: AD_UPDATE_REQUEST,
               data:{
                   ad_title: ad_title,
                   ad_image: adImage?adImage:editAd.image,
                   ad_url:ad_url,
                   id:editAd.id,
                   page:pages.page
               }
            });
        }else {
            dispatch({
                type: AD_ADD_REQUEST,
                data: {
                    ad_title: ad_title,
                    ad_image: adImage,
                    ad_url: ad_url
                }
            });
        }
        console.log(ad_title)
        onHideClick();
    },[ad_title,ad_url,adImage,page,editAd]);


    return (
        <div>
            <div className="ad_box">
                <h3>분양관련 광고 사이트 </h3>
                {
                   user_level===10&&<a href="#" className="newBtn s_newBtn" onClick={onAdClick}>광고등록</a>
                }
                <ul>
                    {
                        ads&&ads.map((ad,key) => {
                          return (
                              <li key={key}>
                                  <Link href={ad.ad_url===null||ad.ad_url===''?"":ad.ad_url} passHref>
                                      <a  target={ad.ad_url===null||ad.ad_url===''?"":"_blank"} rel="noopener noreferrer">

                                          <img className="img_left"
                                               src={ad.ad_image===null?"/img/no_image.png":`http://localhost:3061/uploads/${ad.ad_image}`}
                                               alt={ad.ad_title} width="170" height="170"/>
                                      </a>
                                  </Link>
                                  <p>{ad.ad_title}</p>
                                  {
                                      /* 수정 삭제 관리자만 일 때만 보임*/
                                      user&&user.user_level===10?
                                          <p>
                                              <Button type="button" className="btn-info" onClick={() => {onAdEditClick(ad)}}>수정</Button>
                                              &nbsp;
                                              <Button type="button" onClick={() => {onAdRemoveClick(ad.id)}}>삭제</Button>
                                          </p>:null
                                  }
                              </li>
                          )
                        })
                    }

                </ul>

            </div>
            <div className="text-center">
            <Pagination>
                {
                    pages.startPage!==undefined && 1 < pages.startPage ?
                        <>
                            <Pagination.First/>
                            <Pagination.Prev/>
                        </> :
                        null
                }
                {
                    pages.fromRecordPage&&pages.fromRecordPage
                        .map((_, i) => (
                            <Pagination.Item active={i+1===pages.page} key={i} onClick={onAdListClick}>{i + 1}</Pagination.Item>
                        ))
                }
                {
                    pages.lastPage && pages.lastPage < pages.totalPage?
                        <>
                            <Pagination.Next />
                            <Pagination.Last />
                        </>:null
                }

            </Pagination>
            </div>
            {/* 모달창 띄우기*/}
            <Modal
                show={show}
                onHide={onHideClick}
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>광고등록</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td valign="middle" style={{marginTop:'15px'}}><Form.Label>업체명</Form.Label></td>
                                    <td>
                                        <Form.Control
                                            type="text"
                                            ref={adTitleRef}
                                            value={ad_title}
                                            onChange={onChangeAdTitle}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" style={{marginTop:'15px'}} colSpan="2">
                                        <FileUploader name="file" handleChange={handleChange} types={fileTypes} label="광고이미지를 등록하세요"/>
                                    </td>
                                </tr>
                                {
                                    is_upload?
                                        <tr>
                                            <td>
                                                이미지업로드 성공<br/>
                                                <img src={`${axios.defaults.baseURL}uploads/${adImage}`} width="50"/>
                                            </td>
                                        </tr>:null
                                }
                                {
                                    !is_upload&&editAd&&editAd.ad_image!==null?
                                        <tr>
                                            <td>
                                                분양사진<br/>
                                                <img src={`${axios.defaults.baseURL}uploads/${editAd.ad_image}`} width="50"/>
                                            </td>
                                        </tr>:null
                                }

                                <tr>
                                    <td valign="middle" style={{marginTop:'15px'}}><Form.Label>링크주소</Form.Label></td>
                                    <td>
                                        <Form.Control
                                            type="url"
                                            ref={adUrlRef}
                                            value={ad_url}
                                            onChange={onChangeAdUrl}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <div style={{width:'100%'}}>
                     {
                         editAd?
                             <button className="newBtn" type="button" onClick={onAdSubmitClick}>광고수정</button>
                             :
                             <button className="newBtn" type="button" onClick={onAdSubmitClick}>광고등록</button>
                     }


                    </div>
                </Modal.Footer>
            </Modal>
        </div>
    );
}



export default AdLayout;


