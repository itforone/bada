import React from "react";
import Link from "next/link";

const MainMenu = () => {
    return (
        <div>
            <div id="inb">
                <ul>
                    <li>
                        <Link href="">
                            <a>
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb10.png"
                                        alt="" width="54"/>자유게시판</p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a className="popup">
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb01.png"
                                        alt=""/>분양
                                    MGM 사이트</p>
                            </a>
                        </Link>
                    </li>

                    <li>
                        <Link href="">
                            <a>
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb_crm.png"
                                        alt=""/>전화상담CRM<span>고객관리프로그램</span></p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/fitset">
                            <a>
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb04.png"
                                        alt=""/>핏셋!
                                </p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a className="popup">
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb03.png"
                                        alt=""/>가점검수 솔루션</p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a>
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb05_2.png"
                                        alt=""/>인벤토리!</p>

                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a className="popup">
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb06.png"
                                        alt=""/>해촉증명서발급
                                </p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a>
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb07_2.png"
                                        alt=""/>방문예약시스템</p>
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="">
                            <a className="popup">
                                <p><img src="https://itforone.co.kr:443/~badasinwoo/theme/basic/img/main/inb05.png"
                                        alt=""/>전국
                                    분양정보</p>
                            </a>
                        </Link>
                    </li>

                </ul>
            </div>
        </div>
    );
}

export default MainMenu;