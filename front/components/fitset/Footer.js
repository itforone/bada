import React from "react";
const Footer = () => {
    return (
        <div>
            <div id="ft">
                <div id="ft_copy" className="container">

                    <p>핏셋! 플랫폼</p>

                    <strong>주소</strong> 부산시 해운대구 센텀중앙로 97, 비동 404호(재송동, 센텀스카이비즈)&nbsp;<br/>
                    <strong>사업자등록번호</strong> 427-81-00183
                    <strong>대표자</strong> 김우태,고동국<br/>
                    <strong>이메일</strong> smgholdings@nate.com&nbsp;
                    <strong></strong> &nbsp;
                    <strong></strong> &nbsp;
                    <strong></strong> <br/>
                    <strong></strong> &nbsp;
                    <strong></strong> &nbsp;
                    <strong></strong>
                    <p>COPYRIGHTⓒ2022 FITSET. ALL RIGHTS RESERVED.</p><br/>
                    <a href="#hd" id="ft_totop"><img src="https://dreamforone.co.kr:443/~fitset/theme/basic/img/btn_top.gif" border="0" /></a>
                </div>
            </div>
        </div>
    )
}

export default Footer;