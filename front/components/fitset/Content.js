import React from "react";
const Content = () => {
    return (
        <div>
            <div id="hd_cont" className="container ">
                <div className="row">
                    <div className="col-lg-3 col-xs-3">
                        <img src="https://dreamforone.co.kr:443/~fitset/theme/basic/img/icon_01.png"/>
                        <p className="semi_title"><span>업체등록</span></p>
                        <p className="semi_text">외부홍보<br/>간소화 서비스<br/>
                            핏셋!에 등록하여 <br/>
                            서비스를 이용하세요
                        </p>
                    </div>
                    <div className="col-lg-6 col-xs-6">
                        <img src="https://dreamforone.co.kr:443/~fitset/theme/basic/img/icon_03.png"/>
                        <p className="semi_title"><span>실시간 관리자 파악</span></p>
                        <p className="semi_text">지도내 미방문 또는 실시간 방문체크<br/>
                            확인으로 효율적인 마케팅 실현가능<br/>
                            <span>(지도 내 방문 위치 체크, 담당자, <br/>시간 등 엑셀 다운로드 가능)</span>
                        </p>
                    </div>
                    <div className="col-lg-3 col-xs-3">
                        <img src="https://dreamforone.co.kr:443/~fitset/theme/basic/img/icon_02.png"/>
                        <p className="semi_title"><span>사용자 이용</span></p>
                        <p className="semi_text">핏셋!을 통해 <br/>
                            사용자가 간편하게<br/>
                            등록 가능합니다</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Content;