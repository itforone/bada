import React,{useCallback} from "react";
import Link from "next/link";
import {useDispatch, useSelector} from "react-redux";
import {LOGOUT_REQUEST} from "../../reducers/user";
const Top = () => {
    const {user} = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const onLogoutClick = useCallback(()=>{
        console.log("로그아웃");
        dispatch({
            type:LOGOUT_REQUEST
        });
    },[user]);
    return (
        <div>
            <section id="first">
                <div className="wrap wrap1">
                    <div id="gnb" className="container">
                        <ul>
                            <li><a href="/fitset" target="_self">홈</a></li>
                            {
                                user?
                                    <li>
                                        <a onClick={onLogoutClick}>
                                            로그아웃
                                        </a>
                                    </li>:
                                    <li>
                                        <Link href="/fitset/login">
                                            <a>로그인</a>
                                        </Link>
                                    </li>
                            }

                        </ul>
                    </div>
                    <div id="hd" className="container">
                        <h1><span className="title">핏셋! </span> 플랫폼 서비스</h1>
                        <p>핏셋! 효율적인 마케팅 실현 가능</p>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Top;