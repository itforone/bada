import React, {useCallback} from "react";
import Head from "next/head";
import {useDispatch, useSelector} from "react-redux";
import {LOGOUT_REQUEST} from "../../reducers/user";
import Link from "next/link";
const Header = ({title}) => {



    return (
        <div>
            <Head>
                <title>{title}</title>
                <link rel="stylesheet" href="/css/fitset/style.css"/>
            </Head>

        </div>
    );
}

export default Header;