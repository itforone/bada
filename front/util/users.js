import React,{useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getCookie, setCookie} from "../util/cookie";
import {SIGN_IN_REQUEST, TOKEN_REQUEST} from "../reducers/user";
import AppHeader from "../components/AppHeader";
//로그인 성공할 때 토큰 값 저장
export const signInEffect = (signInDone,user) => {
    useEffect(() => {

        if(signInDone && user){
            console.log(user.access_token);
            setCookie('access_token', user.access_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});
            setCookie('refresh_token', user.refresh_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});
        }
    },[signInDone,user]);
};
//로그아웃 할 때 토큰 값 삭제
export const signOutEffect = (signOutDone) => {
    //로그아웃을 할 때
    useEffect(() => {

        console.log(signOutDone);
        if(signOutDone){
            setCookie('access_token','');
            setCookie('refresh_token','');
        }
    },[signOutDone]);
}
//토큰으로 자동로그인하기
export const tokenSignInEffect = (dispatch) => {
    useEffect(() => {
        console.log(`token:${getCookie('access_token')}`);
        if(getCookie('access_token')) {
            dispatch({
                type: TOKEN_REQUEST,
                data: {
                    refresh_token: getCookie('refresh_token'),
                    access_token: getCookie('access_token'),
                }
            });
        }
    },[]);
}
