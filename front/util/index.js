
import React,{useEffect} from "react";
import { Cookies } from 'react-cookie';

const cookies = new Cookies();

export const setCookie = (name,value,option) =>{
    return cookies.set(name,value,{...option});
}
export const getCookie = (name) =>{
    return cookies.get(name);
}



export const autoHyphone = (value,setInputValue) => {
    useEffect(() => {
        if(value.length === 10){
            setInputValue(value.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3'));
        }
        if(value.length === 13){
            setInputValue(value.replace(/-/g, '').replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3'));
        }
    },[value,setInputValue]);
}
