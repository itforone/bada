import React, {useEffect} from "react";
import Header from "../components/bada/Header";
import Logo from "../components/Logo";
import LoginMenuLayout from "../components/LoginMenuLayout";
import MainMenu from "../components/MainMenu";
import AdLayout from "../components/AdLayout";
import Footer from "../components/Footer";
import {useDispatch, useSelector} from "react-redux";
import {LOAD_USER_REQUEST} from "../reducers/user";
import wrapper from "../store/configureStore";
import axios from 'axios';
import {END} from "redux-saga";
import {AD_LIST_REQUEST} from "../reducers/ad";

const Home = () => {

    const dispatch = useDispatch();
    const {user} = useSelector((state) => state.user);



    return (
        <div>
            {/* 헤더 가져오기 */}
            <Header/>
            {/* 로고 가져오기*/}
            <Logo/>
            <div id="idx_wrapper">
                <div id="idx_container">
                    {/* 로그인 아이디/비번찾기/회원가입 메뉴*/}
                    <LoginMenuLayout user={user}/>
                    {/* 메인 메뉴 */}
                    <MainMenu/>
                    {/* 광고 가져오기 */}
                    <AdLayout/>
                </div>
            </div>
            {/* 풋터 가져오기 */}
            <Footer/>
        </div>
    );
};

export const getServerSideProps = wrapper.getServerSideProps(store => async ({req, res, ...etc}) => {
    const cookie = req ? req.headers.cookie : '';
    axios.defaults.headers.Cookie = '';

    if (req && cookie) {
        axios.defaults.headers.Cookie = cookie;
    }
    console.log(cookie);

    store.dispatch({
        type: LOAD_USER_REQUEST,
    });
    store.dispatch({
        type: AD_LIST_REQUEST,
        data:{page:''}
    });

    store.dispatch(END);
    await store.sagaTask.toPromise();


});


export default Home;

