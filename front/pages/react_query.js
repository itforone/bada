import React from "react";
import {dehydrate, QueryClient, useQuery} from "react-query";
import {fetchPosts} from "../hooks/api/usePosts";
import Link from "next/link";

const ReactQuery = () => {
    const { isLoading, error, data } = useQuery("posts", () =>
        fetchPosts(10)
    );
    console.log(isLoading);
    if (isLoading) return <div>Loading 중</div>
    if (error) return "An error has occurred: " + error?.message;
    return (
        <>
            <div>index</div>
            <Link href="/">
                <a>test페이지로</a>
            </Link>
            <ul>
                {data && data.map((post) => (
                    <li key={post.id}>
                        <div>
                            <span>{post.id}. </span>
                            <span key={post.id}>
                                  <Link href={`/ssr2/${post.id}`}>{post.title}</Link>
                            </span>
                            <a href="#">{post.title}</a>
                        </div>
                    </li>
                ))}
            </ul>
        </>
    );
}

export default ReactQuery;
