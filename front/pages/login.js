import React, {useCallback, useEffect, useRef} from "react";
import {Form} from "react-bootstrap";
import Head from "next/head";
import styled from "styled-components";
import Link from "next/link";
import useInput from "../hooks/useInput";
import {errorAlert} from "../util/sweetalert2";
import {useDispatch, useSelector} from "react-redux";
import {LOGIN_REQUEST, loginRequestAction} from "../reducers/user";
import Router from "next/router";
import axios from "axios";
import { setCookie } from "../util";


const LoginBody = styled.div`
    width:100%;
    height:100vh;
    background:url(https://itforone.co.kr:443/~badasinwoo/theme/basic/skin/member/basic/img/bg.jpg) 0 0 /auto 100%; 
    overflow-y:hidden; 
    overflow-x:hidden;
`;


const Login = () => {
    //useState 설정
    const [user_id,setUserId] = useInput('');
    const [user_password,setUserPassword] = useInput('');
    //ref 설정
    const userIdRef = useRef();
    const userPasswordRef = useRef();
    //디스패치 설정
    const dispatch = useDispatch();
    const {loginError,loginDone,user} = useSelector((state) => state.user);

    //useEffect로 로그인 실패시 메세지 보여주기
    useEffect(() => {
        if(loginError!==null){
           errorAlert(null,'회원로그인오류',loginError);
           return;
        }
        //Router.replace("/");

    },[loginError]);

    useEffect(()=>{
        if(loginDone) {
            //토큰값을 저장을 함
            axios.defaults.headers.common["x-access-token"] = user.access_token;
            //setCookie('access_token','');
            //액세스와 리프레시 토큰값을 쿠키로 저장
            setCookie('access_token', user.access_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});
            setCookie('refresh_token', user.refresh_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});

            Router.replace("/");
            return;
        }
    },[loginDone,user]);



    const onLoginKeyPress = useCallback((e)=>{
        if(e.key==='Enter'){
            onLoginClick();
        }
    },[user_id,user_password]);




    const onLoginClick = useCallback(() => {
        if(user_id.length < 4){
            errorAlert(userIdRef,'아이디 미입력','아이디를 4자리 이상 입력하십시오');
            return;
        }
        if(user_password.length < 4){
            errorAlert(userPasswordRef,'비밀번호 미입력','비밀번호를 4자리 이상 입력하십시오');
            return;
        }
        dispatch(loginRequestAction({user_id,user_password}));
    },[user_id,user_password]);
    return (
        <>
            <Head>
                <title>로그인</title>
                {/* public에 css에서 불러와야 함 */}
                <link rel="stylesheet" href="/css/login.css"/>
            </Head>
            <LoginBody>
                <div id="mb_login" className="mbskin">
                    <h1><img src="/img/common/logo.png" alt="BADA"/></h1>
                    <Form>
                        <fieldset id="login_fs">
                            <p className="id_save">
                                <input type="checkbox" name="id_save" id="id_save"/>
                                <label htmlFor="id_save">아이디저장</label>
                            </p>
                            <legend>로그인</legend>
                            <label htmlFor="login_id" className="login_id">아이디<strong
                                className="sound_only"> 필수</strong></label>
                            <input type="text"
                                   name="user_id"
                                   id="user_id"
                                   className="frm_input"
                                   size="20"
                                   ref={userIdRef}
                                   onChange={setUserId}
                                   maxLength="20"/>
                            <label htmlFor="login_pw" className="login_pw">비밀번호<strong className="sound_only"> 필수</strong></label>
                            <input type="password"
                                   name="user_password"
                                   id="user_password"
                                   className="frm_input"
                                   ref={userPasswordRef}
                                   onChange={setUserPassword}
                                   onKeyUp={onLoginKeyPress}
                                   size="20"
                                   maxLength="20"/>
                            <button className="btn_submit" type="button" onClick={onLoginClick}>로그인</button>
                            <Link href="/">
                                <a className="btn_submit">메인으로</a>
                            </Link>
                        </fieldset>
                    </Form>
                </div>
            </LoginBody>
        </>
    );
}

export default Login;