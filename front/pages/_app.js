import PropTypes from "prop-types";
import React from "react";
import Head from 'next/head';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/font.css';
import '../css/animate.min.css';
import '../css/style1.css';
import '../css/default.css';
import '../css/main.css';
import '../css/common.css';
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";//리액트 쿼리를 쓰려면 이게 필요함


import wrapper from "../store/configureStore";
//_app.js에서 공통된 부분을 처리를 할 수 있음
//pages js파일을 공통된 부분을 props로 가져올 수 있음
const BadaComponent = ({ Component,pageProps }) => {
    const [queryClient] = React.useState(() => new QueryClient());
    return(
        <>
            <Head>
                <meta charSet="utf-8" />
                <title>Bada 플랫폼</title>
            </Head>
            {/* 리액트 쿼리 세팅하기 */}
            <QueryClientProvider client={queryClient}>
                <Hydrate state={pageProps.dehydratedState}>
                    <Component />
                </Hydrate>
            </QueryClientProvider>

        </>
    );
}
BadaComponent.propTypes = {
    Component: PropTypes.elementType.isRequired
}
//export default BadaComponent;
export default wrapper.withRedux(BadaComponent);//리덕스 사용하기 위한 것