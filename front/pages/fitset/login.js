import React,{useCallback, useEffect, useRef} from "react";
import Header from "../../components/fitset/Header";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form} from "react-bootstrap";
import useInput from "../../hooks/useInput";
import {errorAlert} from "../../util/sweetalert2";
import axios from "axios";
import {setCookie} from "../../util";
import Router from "next/router";
import {loginRequestAction} from "../../reducers/user";
const Login = () => {
    //useState 설정
    const [user_id,setUserId] = useInput('');
    const [user_password,setUserPassword] = useInput('');
    //ref 설정
    const userIdRef = useRef();
    const userPasswordRef = useRef();
    //디스패치 설정
    const dispatch = useDispatch();
    const {loginError,loginDone,user} = useSelector((state) => state.user);

    //useEffect로 로그인 실패시 메세지 보여주기
    useEffect(() => {
        if(loginError!==null){
            errorAlert(null,'회원로그인오류',loginError);
            return;
        }
        //Router.replace("/");

    },[loginError]);

    useEffect(()=>{
        if(loginDone) {
            //토큰값을 저장을 함
            axios.defaults.headers.common["x-access-token"] = user.access_token;
            //setCookie('access_token','');
            //액세스와 리프레시 토큰값을 쿠키로 저장
            setCookie('access_token', user.access_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});
            setCookie('refresh_token', user.refresh_token,{path:'/',secure:false,sameSite:"Strict",maxAge: 31536000});

            Router.replace("/fitset");
            return;
        }
    },[loginDone,user]);



    const onLoginKeyPress = useCallback((e)=>{
        if(e.key==='Enter'){
            onLoginClick();
        }
    },[user_id,user_password]);




    const onLoginClick = useCallback(() => {
        if(user_id.length < 4){
            errorAlert(userIdRef,'아이디 미입력','아이디를 4자리 이상 입력하십시오');
            return;
        }
        if(user_password.length < 4){
            errorAlert(userPasswordRef,'비밀번호 미입력','비밀번호를 4자리 이상 입력하십시오');
            return;
        }
        dispatch(loginRequestAction({user_id,user_password}));
    },[user_id,user_password]);

    return (
        <div>
            <Header title={'분양플랫폼 핏셋 로그인'}/>
            <link rel="stylesheet" href="/css/fitset/member.css"/>
            <div id="mb_login" className="mbskin">
                <a href="https://dreamforone.co.kr:443/~fitset/"><h1><img
                    src="https://dreamforone.co.kr:443/~fitset/theme/basic/skin/member/basic/img/logo_white.png"
                    alt="로그인"/></h1></a>

                <Form>
                        <fieldset id="login_fs">
                            <legend>회원로그인</legend>
                            <label htmlFor="login_id" className="login_id">회원아이디<strong
                                className="sound_only"> 필수</strong></label>
                            <input type="text"
                                   name="mb_id"
                                   id="login_id"
                                   className="frm_input"
                                   placeholder="아이디를 입력해 주세요"
                                   ref={userIdRef}
                                   onChange={setUserId}
                                   size="20"
                                   maxLength="20"/>
                            <label htmlFor="login_pw" className="login_pw">비밀번호<strong
                                className="sound_only"> 필수</strong></label>
                            <input type="password"
                                   placeholder="비밀번호를 입력해 주세요"
                                   size="20"
                                   maxLength="20"
                                   className="frm_input"
                                   ref={userPasswordRef}
                                   onChange={setUserPassword}
                                   onKeyUp={onLoginKeyPress}
                            />
                            <Button type="button" className="btn_submit" onClick={onLoginClick}>로그인</Button>
                        </fieldset>

                        <aside id="login_info">

                            <div className="text-center">
                                <a href="/fitset/register" className="register_form">회원 가입</a>
                                <a href="#"
                                   target="_self">
                                    아이디
                                    비밀번호 찾기
                                </a>
                                <br/>

                            </div>
                        </aside>


                        <button className="app" type="button"
                                >영업사원용 어플
                            다운로드<img
                                src="https://dreamforone.co.kr:443/~fitset/theme/basic/skin/member/basic/img/download.png"/>
                        </button>
                </Form>


            </div>


        </div>
    );
}


export default Login;