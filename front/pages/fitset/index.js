import React from "react";
import Header from "../../components/fitset/Header";
import Content from "../../components/fitset/Content";
import Footer from "../../components/fitset/Footer";
import {useSelector} from "react-redux";
import wrapper from "../../store/configureStore";
import axios from "axios";
import {LOAD_USER_REQUEST} from "../../reducers/user";
import {AD_LIST_REQUEST} from "../../reducers/ad";
import {END} from "redux-saga";
import Top from "../../components/fitset/Top";
const Fitset = () => {
    const {user} = useSelector((state) => state.user);

    return (
        <div>
            <Header title={'분양플랫폼 핏셋'}/>
            <Top/>

            <section id="second">
                <div className="wrap wrap2">
                    <div id="" className="container">
                        <button type="button" className="registe btn ">핏셋! 바로가기
                        </button>
                    </div>
                </div>
            </section>
            <Content/>
            <Footer/>


        </div>
    );
}

export const getServerSideProps = wrapper.getServerSideProps(store => async ({req, res, ...etc}) => {
    const cookie = req ? req.headers.cookie : '';
    axios.defaults.headers.Cookie = '';

    if (req && cookie) {
        axios.defaults.headers.Cookie = cookie;
    }
    console.log(cookie);

    store.dispatch({
        type: LOAD_USER_REQUEST,
    });
    store.dispatch(END);
    await store.sagaTask.toPromise();


});
export default Fitset;